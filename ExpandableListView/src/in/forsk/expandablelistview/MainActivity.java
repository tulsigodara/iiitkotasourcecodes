package in.forsk.expandablelistview;

import in.forsk.expandablelistview.adapter.CustomExpandableListAdapter;
import in.forsk.expandablelistview.wrapper.FacultyWrapper;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Debug;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends Activity {
	private final static String TAG = MainActivity.class.getSimpleName();
	Context context;
	// Expandable List view reference
	ExpandableListView lv;
	
	
	// (Array List holding objects of FacultyWrapper)
	ArrayList<FacultyWrapper> mFacultyDataList;

	//Header data model
	ArrayList<String> headerData = new ArrayList<String>();
	
	//Childlist data model
	HashMap<String, ArrayList<FacultyWrapper>> ChildListData = new HashMap<String, ArrayList<FacultyWrapper>>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		context = this;

		// Creating reference
		lv = (ExpandableListView) findViewById(R.id.expandableListView);

		// Local File Parsing
		try {
			String json_string = getStringFromRaw(context, R.raw.faculty_profile_code);

			mFacultyDataList = pasreLocalFacultyFile(json_string);

			// setFacultyListAdapter(mFacultyDataList);
		} catch (IOException e) {
			e.printStackTrace();
		}

		//This method will filter data according to expandable list view needs
		//after this method the data in header data model and child data model
		filterDataForExpandableList();

		// creating adapter as a bridge between tha data model and view
		// We pass context and data model in our custom made adapter constructor
		// so we can use them there
		// we need context to init the layout inflater service , which inflate
		// the custom Views(Resource layout)
		CustomExpandableListAdapter adapter = new CustomExpandableListAdapter(context, headerData, ChildListData);

		// Setting adapter to the list view, at this point list view use adapter
		// class methods to fill its view start the recycling process.
		lv.setAdapter(adapter);

		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				Log.d(TAG, "getNativeHeapAllocatedSize - " + Debug.getNativeHeapAllocatedSize());
				Log.d(TAG, "getNativeHeapSize - " + Debug.getNativeHeapSize());
				Log.d(TAG, "getNativeHeapFreeSize - " + Debug.getNativeHeapFreeSize());
			}
		}, 2000);
	}

	private void filterDataForExpandableList() {
		
		for (FacultyWrapper obj : mFacultyDataList) {
			if (headerData.contains(obj.getDepartment())) {
				
				ArrayList<FacultyWrapper> mtempList = ChildListData.get(obj.getDepartment());
				mtempList.add(obj);
				ChildListData.put(obj.getDepartment(), mtempList);
				
			} else {
				headerData.add(obj.getDepartment());

				ArrayList<FacultyWrapper> mtempList = new ArrayList<FacultyWrapper>();
				mtempList.add(obj);
				ChildListData.put(obj.getDepartment(), mtempList);
			}
		}
	}

	private String getStringFromRaw(Context context, int resourceId) throws IOException {
		// Reading File from resource folder
		Resources r = context.getResources();
		InputStream is = r.openRawResource(resourceId);
		String statesText = convertStreamToString(is);
		is.close();

		Log.d(TAG, statesText);

		return statesText;
	}

	private String convertStreamToString(InputStream is) throws IOException {
		// Converting input stream into string
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		int i = is.read();
		while (i != -1) {
			baos.write(i);
			i = is.read();
		}
		return baos.toString();
	}

	public ArrayList<FacultyWrapper> pasreLocalFacultyFile(String json_string) {

		ArrayList<FacultyWrapper> mFacultyDataList = new ArrayList<FacultyWrapper>();
		try {
			// Converting multipal json data (String) into Json array
			JSONArray facultyArray = new JSONArray(json_string);
			Log.d(TAG, facultyArray.toString());
			// Iterating json array into json objects
			for (int i = 0; facultyArray.length() > i; i++) {

				// Extracting json object from particular index of array
				JSONObject facultyJsonObject = facultyArray.getJSONObject(i);

				// Design patterns
				FacultyWrapper facultyObject = new FacultyWrapper(facultyJsonObject);

				printObject(facultyObject);

				mFacultyDataList.add(facultyObject);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mFacultyDataList;
	}

	public void printObject(FacultyWrapper obj) {
		// Operator Overloading
		Log.d(TAG, "First Name : " + obj.getFirst_name());
		Log.d(TAG, "Last Name : " + obj.getLast_name());
		Log.d(TAG, "Photo : " + obj.getPhoto());
		Log.d(TAG, "Department : " + obj.getDepartment());
		Log.d(TAG, "reserch_area : " + obj.getReserch_area());
		Log.d(TAG, "Phone : " + obj.getPhone());
		Log.d(TAG, "Email : " + obj.getEmail());

		for (String s : obj.getInterest_areas()) {
			Log.d(TAG, "Interest Area : " + s);
		}
	}
}
