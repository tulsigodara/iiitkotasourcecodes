package in.forsk.servicesandbroadcastreceivers;

import in.forsk.servicesandbroadcastreceivers.DownloadResultReceiver.Receiver;

import java.io.File;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends Activity {

	Context context;
	ImageView imageView;
	Button loadBtn, btn2;

	private ResultReceiver mReceiver;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		/* Allow activity to show indeterminate progressbar */
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);

		setContentView(R.layout.activity_main);

		context = this;

		imageView = (ImageView) findViewById(R.id.imageView1);
		loadBtn = (Button) findViewById(R.id.button1);

		loadBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// new ImageLoaderAsyncTask(context,
				// "http://iiitkota.forsklabs.in/appLogo.png",
				// imageView).execute();
				// 44444444444444444444
				// new
				// AQuery(context).id(imageView).image("http://iiitkota.forsklabs.in/appLogo.png");

			}
		});

		btn2 = (Button) findViewById(R.id.button2);
		btn2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/* Starting Download Service */
				Intent intent = new Intent(context, DownloadService.class);

				//Parceable concept is use to exchange the data between the class(through intent)
				/* Send optional extras to Download IntentService */
				intent.putExtra("url", "http://firozstar.tripod.com/_darksiderg.pdf");
				// intent.putExtra("receiver", mReceiver);
				intent.putExtra("requestId", 101);

				startService(intent);
			}
		});

		// We need to register broadcast receiver on the start of the activity
		// need to be unregister at end of the lifecycle of the activity

		mReceiver = new ResultReceiver();

		// To receive the broadcast from teh system, we need define which
		// broadcast android need to send our app
		IntentFilter filter = new IntentFilter("in.forsk.update");
		registerReceiver(mReceiver, filter);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		// We need to register broadcast receiver on the start of the activity
		// need to be unregister at end of the lifecycle of the activity
		unregisterReceiver(mReceiver);
	}

	//This has been created as inner class so that within the class we can access button member variable 
	//This will run on the UI main thread, so accessing the UI component becomes easy
	public class ResultReceiver extends BroadcastReceiver {

		//this method is called every time, when our app receiver a broadcast for intent filter (in.forsk.update)
		@Override
		public void onReceive(Context context, Intent intent) {

			//Parceable concept is use to exchange the data between the class(through intent)
			int resultCode = intent.getIntExtra("resultCode", -1);
			Bundle resultData = intent.getExtras();
			
			switch (resultCode) {
			case DownloadService.STATUS_RUNNING:

				//You can start a progress bar here
				
				break;
			case DownloadService.STATUS_FINISHED:
				/* Hide progress bar & extract result from bundle */
//				setProgressBarIndeterminateVisibility(false);

				final String results = resultData.getString("result");

				//On result ,change the button text and set a new click listener to open the PDF file
				btn2.setText("Open PDF file");
				btn2.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						
						File file = new File(results);
						
						//View intent to view the PDF file
						Intent target = new Intent(Intent.ACTION_VIEW);
						//Set the Data type we have
						target.setDataAndType(Uri.fromFile(file), "application/pdf");
						//set the flag as we dont want to keep history of app stack
						target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

						//this will show popup to choose available PDF readers
						Intent intent = Intent.createChooser(target, "Open File");
						try {
							//Fire the intent
							startActivity(intent);
						} catch (ActivityNotFoundException e) {
							// Instruct the user to install a PDF reader here,
							// or
							// something
						}
					}
				});

				break;
			case DownloadService.STATUS_ERROR:
				/* Handle the error */
				String error = resultData.getString(Intent.EXTRA_TEXT);
				break;
			}
		}

	}

}
