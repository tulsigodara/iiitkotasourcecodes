package in.forsk.listviewwitharrayadapter;

import android.app.Activity;
import android.os.Bundle;
import android.os.Debug;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends Activity {
	private final static String TAG = MainActivity.class.getSimpleName();
	// List view reference
	ListView lv;

	// /Data model
	String[] data_array = new String[] { "Dr. Bharavi Mishra", "Mr. Dinesh Khandelwal", "Mr. Mukesh K Jadon", "Dr. Poonam", "Dr. PRAVEEN KUMAR", "Dr. Preety Singh", "Dr. Rajbir Kaur",
			"Dr. Rajni Aron", "Dr. RATNADIP ADHIKARI", "Prof. Ravi Prakash Gorthi", "Dr. Sakthi Balan Muthiah", "Mrs. Sonam Nahar", "Dr. Subrat Kumar Dash", "Mr. Sunil Kumar", "Dr. VIBHOR KANT",
			"Mr. Vikas Bajpai" };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// Creating reference
		lv = (ListView) findViewById(R.id.listView1);

		// creating array adapter as a bridge between tha data model and view
		// For instance we are using android provided inbuilt layout
		// Please note android.R.layout vs R.layout
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, android.R.id.text1, data_array);
		// Setting adapter to the list view, at this point list view use adapter
		// class methods to fill its view start the recycling process.
		lv.setAdapter(adapter);

		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
				// TODO Auto-generated method stub
				int itemPosition = position;

				// ListView Clicked item value
				String itemValue = (String) lv.getItemAtPosition(position);

				// Show Alert
				Toast.makeText(getApplicationContext(), "Position :" + itemPosition + "  ListItem : " + itemValue, Toast.LENGTH_LONG).show();
			}
		});

		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				Log.d(TAG, "getNativeHeapAllocatedSize - " + Debug.getNativeHeapAllocatedSize());
				Log.d(TAG, "getNativeHeapSize - " + Debug.getNativeHeapSize());
				Log.d(TAG, "getNativeHeapFreeSize - " + Debug.getNativeHeapFreeSize());
			}
		}, 2000);

	}
}
